---
layout: page
title: WhatsApp
author: mfc
language: my
summary: ဆက်သွယ်ရန် နည်းလမ်းများ
date: 2021-03
permalink: /my/contact-methods/whatsapp.md
parent: /my/
published: true
---

WhatsApp အသုံးပြုခြင်းအားဖြင့် သင့်စကားပြောဆိုမှုများကို ကာကွယ်နိုင်ရန် သင်နှင့်လက်ခံသူများသာ ဖတ်ရှုနိုင်ပါသည်။ သို့သော်လည်း သင်မှ လက်ခံသူအား ဆက်သွယ်ထားသည့် လုပ်ဆောင်ချက်ကို အစိုးရများနှင့် ဥပဒေစိုးမိုးရေးအေဂျင်စီများက သိရှိနိုင်ပါသည်။

လေ့လာရန် - [WhatsApp ကို Android ပေါ်တွင်မည်သို့သုံးရန်](https://ssd.eff.org/en/module/how-use-whatsapp-android) , [WhatsApp ကို iOS ပေါ်တွင်မည်သို့သုံးရန်](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
