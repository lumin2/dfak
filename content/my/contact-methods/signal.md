---
layout: page
title: Signal
author: mfc
language: my
summary: ဆက်သွယ်ရန် နည်းလမ်းများ
date: 2021-03
permalink: /my/contact-methods/signal.md
parent: /my/
published: true
---

Using Signal will ensure the content of your message is encrypted only to the recipient organization, and only you and your recipient will know the communications took place. Be aware that Signal uses your phone number as your username so you will be sharing your phone number with the organization you are reaching out to.

လေ့လာရန် - [Signal ကို Android ပေါ်တွင်မည်သို့သုံးရန်](https://ssd.eff.org/en/module/how-use-signal-android), [Signal ကို iOS ပေါ်တွင်မည်သို့သုံးရန်](https://ssd.eff.org/en/module/how-use-signal-ios), [Signal ကို သင့်ဖုန်းနံပါတ် မထုတ်ဖေါ်ပဲ သုံးရန်](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
