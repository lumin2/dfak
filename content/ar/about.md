---
layout: page.pug
title: "About"
language: ar
summary: "عن عدَّ الإسعاف الأوَلي الرقمي"
date: 2019-03-13
permalink: /ar/about/
parent: Home
---

عدَّة الإسعاف الأوَّلي الرقمي نتيجة تعاون بين [RaReNet (Rapid Response Network)](https://www.rarenet.org) و&nbsp;[CiviCERT](https://www.civicert.org).

شبكة الاستجابة السريعة (Rapid Response Network) شبكة عالمية من المسعِفين و&nbsp;خبراء الأمان الرقمي، تضمّ EFF و&nbsp;Global Voices و&nbsp;Hivos & the Digital Defenders Partnership و&nbsp;Front Line Defenders و&nbsp;Internews و&nbsp;Center for Digital Resilience و&nbsp; La Labomedia و&nbsp;Freedom House و&nbsp;Access Now و&nbsp;Virtual Road و&nbsp;CIRCL و&nbsp;Open Technology Fund و&nbsp;Greenhost بالإضافة إلى أفراد خبراء في الأمان في مجال الأمان الرقمي و&nbsp;الاستجابة الطارئة.

بعض هذه المنظَّمات و&nbsp;هؤلاء الأفراد أعضاء في CiviCERT، و&nbsp;هي شبكة عالمية مِنْ مقدّمي الدعم في الأمان الرقمي و&nbsp;مقدّمي البنية التحتيّة الذين يركِّزون على دعم المجموعات و&nbsp;المنظَّمات الساعية إلى العدالة الاجتماعية و&nbsp;الدفاع عن حقوق الإنسان و&nbsp;الحقوق الرقمية. و&nbsp;سِڤيسٍرت إطار تنظيمي لجهود فرق الاستجابة لحوادث الحواسيب (CERT)، و&nbsp;هي معتمدة مِنْ قِبَل الشبكة الأوربية لفرق الاستجابة لحوادث الحواسيب (Trusted Introducer)

[عدَّة الإسعاف الأوَّلي الرقمي مفتوحة المصدر و&nbsp;نحن نقبل مساهماتكم في تحسينها](https://gitlab.com/rarenet/dfak)

[Digital First Aid Kit](https://www.digitalfirstaid.org/dfak-offline.zip) في السياقات التي يكون فيها الاتصال محدودًا ، أو يكون من الصعب العثور على اتصال ، يمكنك تنزيل إصدار غير متصل بالإنترنت هنا.

للحصول على أي تعليق أو اقتراح أو سؤال حول Digital First Aid Kit ، يمكنك الكتابة إلى: dfak @ digitaldefenders. غزاله

GPG - بصمة: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
