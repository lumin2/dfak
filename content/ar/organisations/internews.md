---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, عالميًّا
response_time: 12 ساعة
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: ‬4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0‬
initial_intake: yes
---

تعمل إنْتَرنيُوز مع اﻷفراد و&nbsp;المنظَّمات و&nbsp;المجتمعات في أنحاء العالم لزيادة الوعي بمسائل الأمان الرقمي، و&nbsp;الدفاع عن النفاذ المفتوح إلى إنترنت بلا رقابة، و&nbsp;تحسين ممارسات الأمان الرقمي. درَّبت إنْتَرنيُوز صحافيين و&nbsp;مدافعين حقوقيّين عديدين في أكثر من 80 بلدًا، و&nbsp;لديها شبكة مِنْ مدرّبي الأمان الرقمي و&nbsp;المراجعين الملمّين بإطار مراجعة الأمان و&nbsp;قالب التقييم لمجموعات المناصرة (SAFETAG) الذي قادت تطويره. كما تقدّم إنْتَرنيُوز تدخّلات تقنية و&nbsp;غير تقنية، منها التقييم اﻷساسي للأمان إلى تقييم السياسات المؤسَّسية، كما نقدّم خدمات تحليل هجمات التصيُّد و&nbsp;البرمجيّات الخبيثة على المدافعين عن حقوق الإنسان و&nbsp;المجموعات الإعلامية مِمَّن يتعرّضون لها.
