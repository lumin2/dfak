---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 12 hours
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Complementing Internews's core work, Internews also works with individuals, organizations, and communities around the world to increase digital security awareness, protect access to an open and uncensored Internet, and improve digital safety practices. Internews has trained journalists and human rights defenders in 80+ countries, and has strong networks of local and regional digital security trainers and auditors familiar with the Security Auditing Framework and Evaluation Template for Advocacy Groups (SAFETAG) framework (https://safetag.org), which Internews has led the development of. Internews is building strong, responsive partnerships with both civil society and private sector threat intelligence and analysis firms, and can support partners directly in maintaining an online, safe, and uncensored presence. Internews offers technical and non-technical interventions alike from baseline security assessments using the SAFETAG framework to organizational policy assessments to revised mitigation strategies based on threat research. They directly support phishing and malware analysis for human rights and media groups alike who experience targeted digital attacks. 
