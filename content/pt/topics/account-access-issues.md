---
layout: page
title: "Não consigo acessar minhas contas"
author: RaReNet
language: pt
summary: "Está tendo problemas ao acessar um email, mídia social ou a conta de um site? Sua conta mostra acessos que você não sabe de onde vêm? Existem várias coisas que você pode fazer para restringir estes problemas."
date: 2015-08
permalink: /pt/topics/account-access-issues/
parent: /pt/
---


# Não consigo acessar minhas contas

Contas de mídias sociais e aplicativos de chat e email são frequentemente utilizadas por pessoas da sociedade civil para comunicarem-se, trocarem conhecimentos e promover suas causas. Como consequência, estas contas são bastante visadas por malfeitores, que geralmente tentam prejudicar o acesso e as informações destas contas, causando prejuízos a estes membros da sociedade civil e seus contatos.

Este guia existe para ajudar no caso de alguma de suas contas ter sido comprometida.

Siga o questionário para identificar a natureza do problema e achar possíveis soluções.

## Workflow

### Password_Typo

> Às vezes não conseguimos entrar com a nossa senha no site porque podemos estar errando a digitação, ou o CapsLock foi ligado sem querer, ou mesmo quando estamos em outro dispositivo, a linguagem do teclado seja diferente daquele que usamos diariamente.
>
> Para verificar, tente abrir um editor de texto ou bloco de notas e digitar seu usuário e sua senha, confira, e cole no navegador ou aplicativo. Verifique também se o CapsLock está ligado (normalmente uma luz indica isto, mas nem todos os teclados possuem), ou se a linguagem do teclado está correta - indicada geralmente na barra do sistema, perto do relógio.

As sugestões acima ajudaram a entrar na sua conta?

- [Sim](#resolved_end)
- [Não](#What_Type_of_Account_or_Service)

### What_Type_of_Account_or_Service

Qual tipo de conta você está tentando acessar?

- [Facebook](#Facebook)
- [Página do Facebook](#Facebook_Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [Protonmail](#Protonmail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service_Name) -->


### Facebook_Page

Sua página possui outras pessoas como admins?

- [Sim](#Other_admins_exist)
- [Não](#Facebook_Page_recovery_form)

### Other_admins_exist

Estas pessoas estão com o mesmo problema?

- [Sim](#Facebook_Page_recovery_form)
- [Não](#Other_admin_can_help)

### Other_admin_can_help

> Tente contatar estas pessoas para que tentem te adicionar como admin da página novamente.

Isso corrigiu o problema?

- [Sim](#Fb_Page_end)
- [Não](#account_end)


### Facebook_Page_recovery_form

> Por favor, entre no Facebook e use [este formulário de recuperação de páginas](https://www.facebook.com/help/contact/164405897002583)).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I_have_access_to_recovery_email_google)
- [Não](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Confira seu e-mail de recuperação. Você recebeu um email do Google dizendo "Alerta de segurança para sua Conta do Google vinculada"?

- [Sim](#Email_received_google)
- [Não](#Recovery_Form_google)

### Email_received_google

Dentro do e-mail existe um link "Recuperar sua conta"?

- [Sim](#Recovery_Link_Found_google)
- [Não](#Recovery_Form_google)

### Recovery_Link_Found_google

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery_Form_google)

### Recovery_Form_google

> Por favor, tente preencher [este formulário para recuperar sua conta](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I_have_access_to_recovery_email_yahoo)
- [Não](#Recovery_Form_Yahoo)

### I_have_access_to_recovery_email_yahoo

Confira seu e-mail de recuperação. Você recebeu um email do Yahoo dizendo "Alterar a senha da sua conta Yahoo"?

- [Sim](#Email_received_yahoo)
- [Não](#Recovery_Form_Yahoo)

### Email_received_yahoo

Dentro do e-mail existe um link "Recupere sua conta aqui"?

- [Sim](#Recovery_Link_Found_Yahoo)
- [Não](#Recovery_Form_Yahoo)

### Recovery_Link_Found_Yahoo

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery_Form_Yahoo)

### Recovery_Form_Yahoo

> Por favor, tente seguir [estas instruções para recuperar sua conta](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I_have_access_to_recovery_email_Twitter)
- [Não](#Recovery_Form_Twitter)

### I_have_access_to_recovery_email_Twitter

Confira seu e-mail de recuperação. Você recebeu um email do Twitter dizendo "Sua senha do Twitter foi alterada"?

- [Sim](#Email_received_Twitter)
- [Não](#Recovery_Form_Twitter)

### Email_received_Twitter

Dentro do e-mail existe um link "Recupere sua conta"?

- [Sim](#Recovery_Link_Found_Twitter)
- [Não](#Recovery_Form_Twitter)

### Recovery_Link_Found_Twitter

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery_Form_Twitter)

### Recovery_Form_Twitter

>Por favor, tente seguir [este formulário para recuperar sua conta](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### Protonmail

> Por favor, tente seguir [este formulário para recuperar sua conta](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I_have_access_to_recovery_email_Hotmail)
- [Não](#Recovery_Form_Hotmail)

### I_have_access_to_recovery_email_Hotmail

Confira seu e-mail de recuperação. Você recebeu um email da Microsoft dizendo "Alteração de senha da sua conta Microsoft"?

- [Sim](#Email_received_Hotmail)
- [Não](#Recovery_Form_Hotmail)

### Email_received_Hotmail

Dentro do e-mail existe um link "Recupere sua senha"?

- [Sim](#Recovery_Link_Found_Hotmail)
- [Não](#Recovery_Form_Hotmail)

### Recovery_Link_Found_Hotmail

> Please use the "Reset your password" link to recover your account.

Were you able to recover your account with "Reset your password" link?

- [Sim](#resolved_end)
- [Não](#Recovery_Form_Hotmail)

### Recovery_Form_Hotmail

> Por favor, tente seguir [este formulário para recuperar sua conta](https://account.live.com/acsr).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


### Facebook

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I_have_access_to_recovery_email_Facebook)
- [Não](#Recovery_Form_Facebook)

### I_have_access_to_recovery_email_Facebook

Confira seu e-mail de recuperação. Você recebeu um email do Facebook dizendo "Alteração de senha do Facebook"?

- [Sim](#Email_received_Facebook)
- [Não](#Recovery_Form_Facebook)

### Email_received_Facebook

Dentro do e-mail existe uma mensagem que pergunta "Foi você?", com um link para tomar medidas de segurança?

- [Sim](#Recovery_Link_Found_Facebook)
- [Não](#Recovery_Form_Facebook)

### Recovery_Link_Found_Facebook

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery_Form_Facebook)

### Recovery_Form_Facebook

> Por favor, tente seguir [este formulário para recuperar sua conta](https://www.facebook.com/login/identify).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I_have_access_to_recovery_email_Instagram)
- [Não](#Recovery_Form_Instagram)

### I_have_access_to_recovery_email_Instagram

Confira seu e-mail de recuperação. Você recebeu um email do Microsoft dizendo "Sua senha do Instagram foi alterada"?

- [Sim](#Email_received_Instagram)
- [Não](#Recovery_Form_Instagram)

### Email_received_Instagram

Dentro do e-mail existe um link de recuperação de senha?

- [Sim](#Recovery_Link_Found_Instagram)
- [Não](#Recovery_Form_Instagram)

### Recovery_Link_Found_Instagram

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery_Form_Instagram)

### Recovery_Form_Instagram

> Por favor, tente seguir [este formulário para recuperar sua conta](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


### Fb_Page_end

Estamos felizes que seu problema foi resolvido! Leia as recomendações a seguir para diminuir as possibilidades de perder acesso novamente no futuro:

- Certifique-se de que todas as pessoas que administram a página ativem a autenticação em dois fatores nas suas contas.
- Garanta que apenas pessoas da sua confiança e que estejam ativamente engajadas com a página recebam acesso de administradoras.

### account_end

Se os procedimentos sugeridos neste diagnóstico não foram capazes de ajudar você a recuperar a conta, procure uma das seguintes organizações capazes de auxiliar com os próximos passos:

:[](organisations?services=account)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Leia as recomendações a seguir para diminuir as possibilidades de perder acesso novamente no futuro:

- Uma boa dica é ativar a autenticação em dois fatores (2FA) sempre que estiver disponível para as suas contas pessoais.
- Nunca use a mesma senha duas vezes. Troque assim que possível as senhas repetidas.
- Usar um programa gerenciador de senhas vai te ajudar a guardar e criar senhas diferentes e mais fortes.
- Tenha cuidado com as informações que você irá acessar em redes públicas (wifi aberto, LAN house etc), e sempre que possível utilize uma VPN confiável ou o navegador Tor nos seus dispositivos.

#### resources

- [Access Now Helpline Community Documentation: Recommendations on Team Password Managers](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html)
- [Security Self-Defense: Como se proteger nas redes sociais](https://ssd.eff.org/pt-br/module/como-se-proteger-nas-redes-sociais)
- [Security Self-Defense: Criando senhas fortes](https://ssd.eff.org/pt-br/module/criando-senhas-fortes)





<!--- Se quiser criar um novo fluxo é só colar e editar o exemplo abaixo, substituindo o service_name pelo nome real do serviço utilizado:
#### service_name

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I_have_access_to_recovery_service_name)
- [Não](#Recovery_Form_service_name)

### I_have_access_to_recovery_email_service_name

Confira seu e-mail de recuperação. Você recebeu um email [nome do serviço] dizendo "[assunto do e-mail]"?

- [Sim](#Email_received_service_name)
- [Não](#Recovery_Form_service_name

### Email_received_service_name

> Dentro do e-mail existe um link "recuperar sua senha"?


- [Sim](#Recovery_Link_Found_service_name)
- [Não](#Recovery_Form_service_name)

### Recovery_Link_Found_service_name

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery_Form_service_name)

### Recovery_Form_service_name

> Por favor, tente seguir [este formulário para recuperar sua conta](link para o formulário de recuperação de senha do serviço).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

-->
