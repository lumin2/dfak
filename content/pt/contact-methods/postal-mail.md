---
layout: page
title: Mensagem Postal
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/postal-mail.md
parent: /pt/
published: true
---

Enviar uma carta é uma forma de comunicação lenta se você está lidando com uma situação urgente. Dependendo da jurisdição pela qual a postagem trafega, autoridades podem abrir a carta, e frequentemente é possível rastrear quem remeteu, a localização de envio, a quem se destina e sua localização.
