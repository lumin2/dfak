---
layout: page.pug
title: "Në lidhje me"
language: sq
summary: "Për Veglërinë e ndihmës së parë digjitale"
date: 2019-03-13
permalink: /sq/about/
parent: Home
---

Veglëria e ndihmës së parë digjitale është përpjekje e përbashkët e [RaReNet (Rapid Response Network)](https://www.rarenet.org/) dhe [CiviCERT](https://www.civicert.org/). Versionet në maqedonisht dhe shqip të këtij produkti janë bërë me mbështetjen e Projektit të Sigurisë dhe Kapacitetit të Informacionit [ISC](https://www.iscproject.org/) dhe realizuar nga Fondacioni Metamorphosis.

Rrjeti i Reagimit të Shpejtë (Rapid Response Network) është një rrjet ndërkombëtar i reaguesve të shpejtë dhe kampionëve të sigurisë digjitale, i cili përfshin EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, Open Technology Fund, Greenhost, si dhe ekspertë individualë të sigurisë që punojnë në fushën e sigurisë digjitale dhe reagimit të shpejtë.

Disa nga këto organizata dhe individë janë pjesë e CiviCERT, një rrjet ndërkombëtar për ndihmë për sigurinë digjitale dhe i ofruesve të infrastrukturës që janë përqendruar kryesisht në mbështetjen e grupeve dhe organizatave që synojnë drejtësinë sociale dhe mbrojtjen e të drejtave njerëzore dhe digjitale. CiviCERT është një grup profesional për përpjekjet e shpërndara të CERT (Computer Emergency Response Team) për reagim të shpejtë të komunitetit. CiviCERT është akredituar nga Trusted Introducer, rrjeti evropian i ekipeve të besueshme për reagim të shpejtë.

Veglëria e ndihmës së parë digjitale është gjithashtu një [projekt me burim të hapur që pranon kontribute të jashtme.](https://gitlab.com/rarenet/dfak)

Nëse dëshironi të përdorni Veglërinë e ndihmës së parë digjitale në kontekstet ku lidhja është e kufizuar, ose gjetja e një lidhjeje është e vështirë, mund të shkarkoni versionin offline [këtu] (https://digitalfirstaid.org/dfak-offline.zip).

Për çdo koment, sugjerim ose pyetje në lidhje me Veglërinë për ndihmën e parë digjitale, mund të shkruani në: dfak @ digitaldefenders . org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
