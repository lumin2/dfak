---
layout: sidebar.pug
title: "Dikush që e njoh është arrestuar"
author: Peter Steudtner, Shakeeb Al-Jabri
language: sq
summary: "Një mik, koleg apo anëtar i familjes është arrestuar nga forcat e sigurisë. Dëshironi ta kufizoni ndikimin e arrestimit të tyre mbi ta dhe këdo tjetër që mund të jetë përzier me ta."
date: 2019-03-13
permalink: /sq/arrested/
parent: Home
sidebar: >
	<h3>Lexoni më shumë se çfarë të bëni nëse dikush që ju e njihni është arrestuar:</h3>
    
	 <ul>
  <li><a href="https://www.newtactics.org/search/solr/arrest">Marrja e mbështetjes për zhvillimin e fushatës në emër të personit të arrestuar</a></li>
	</ul>

---

# Dikush që unë e njoh është arrestuar

Arrestimet e mbrojtësve të të drejtave të njeriut, gazetarëve dhe aktivistëve i vendosin ata dhe personat me të cilët punojnë dhe jetojnë në një rrezik të madh.

Ky udhëzues është i orientuar posaçërisht për vendet që kanë mungesë të respektimit të të drejtave të njeriut dhe procesit të rregullt ligjor, ose ku autoritetet mund të anashkalojnë procedurat ligjore, ose ku aktorët jo-shtetërorë veprojnë lirshëm, paraburgimet ose arrestimet paraqesin një rrezik më të madh për viktimat, kolegët dhe të afërmit e tyre.

Në këtë udhëzues synojmë të lehtësojmë rrezikun me të cilin përballen ata dhe të kufizojmë mundësinë e qasjes nga ana e autoriteteve arrestuese në të dhënat e ndjeshme që mund të inkriminojnë viktimat dhe kolegët e tyre, ose që mund të përdoren për të komprometuar operacionet e tjera.

Kujdesi për të arrestuarën si dhe për ndikimet digjitale të këtij paraburgimi mund të jetë rraskapitës dhe sfidues. Mundohuni të gjeni njerëz të tjerë që t'ju mbështesin dhe të koordinoni veprimet tuaja me komunitetin e përfshirë.

Gjithashtu është e rëndësishme që të kujdeseni për veten tuaj dhe të tjerët që janë të ndikuar nga ky arrestim, duke:

- [u kujdesur për mirëqenien dhe nevojat tuaja psiko-sociale](../self-care),
<!-- - [taking care of the legal side of your support work](***link to RaReNet section on organizations helping on legal matters***) -->
- [marrja e mbështetjes për zhvillimin e fushatës në emër të personit të arrestuar](https://www.newtactics.org/search/solr/arrest)

Nëse ndiheni të mbingarkuar teknikisht ose emocionalisht ose (për çfarëdo arsye) nuk jeni në gjendje të ndiqni hapat e përshkruar më poshtë, ju lutemi kërkoni ndihmë dhe udhëzime nga organizatat e renditura [këtu] (../support) që ofrojnë përzgjedhje fillestare midis shërbimeve të tyre.


## Bëni një plan

Para se të veproni në seksionet e ndryshme që i përshkruajmë më poshtë, ju lutemi kaloni hapat e mëposhtëm:

- Mundohuni ta lexoni të gjithë udhëzuesin dhe të merrni një pasqyrë të të gjitha fushave të rëndësishme të ndikimit përpara se të merrni masa në aspektet e veçanta. Arsyeja është se seksionet e ndryshme nxjerrin në pah skenarë të ndryshëm të kërcënimeve të cilat mund të përputhen, kështu që do t'ju duhet të ndërtoni sekuencën tuaj të veprimeve.
- Ndani kohë me miqtë ose me ekipin për të kryer vlerësime të ndryshme të rrezikut që janë të nevojshme në seksione të ndryshme.

<a name="harm-reduction"></a>
## Bëhuni gati para se të veproni

A keni arsye të besoni se ky arrestim ose paraburgim mund të çojë në pasoja për familjarët, miqtë, ose kolegët e të arrestuarës, duke përfshirë edhe veten tuaj?

Ky udhëzues do t'ju njoftojë me një seri hapash për të ndihmuar në ofrimin e zgjidhjeve që mund të ndihmojnë në zvogëlimin e ekspozimit të të arrestuarës dhe të kujtdo tjetër që është i përfshirë me ta.

**Këshilla për përgjigje të koordinuar**:

Në të gjitha situatat kur një person është arrestuar, gjëja e parë që duhet të theksohet është se shpesh kur ndodhin incidente të tilla, disa miq ose kolegë reagojnë menjëherë, duke rezultuar ose në përpjekje të dyfishta ose kontradiktore. Pra, është e rëndësishme të mbani mend se koordinimi dhe veprimi i bashkërenduar, në nivelin lokal dhe ndërkombëtar, janë të domosdoshëm si për të mbështetur të arrestuarën dhe për t'u kujdesur për të gjithë të tjerët brenda rrjeteve të tyre të mbështetjes, familjes dhe miqve.

- Krijoni një ekip krize që do të koordinojë të gjitha aktivitetet e mbështetjes, kujdesit, fushatave, etj.
- Përfshini anëtarët e familjes, partnerët, etj., sa më shumë që të jetë e mundur (duke respektuar kufijtë e tyre nëse, për shembull, ata janë të dërrmuar).
- Vendosni qëllime të qarta për fushatën tuaj mbështetëse (dhe shqyrtoni ato shpesh): për shembull, ju mund të dëshironi të arrini lirimin e të arrestuarës, të siguroni mirëqenien e tyre, ose të mbroni familjen dhe përkrahësit dhe të siguroni mirëqenien e tyre si qëllime tuaja të menjëhershme.
- Pajtohuni për kanale të sigurta të komunikimit dhe shpeshtësinë e komunikimit, si dhe kufizimet (p.sh. asnjë komunikim ndërmjet orës 10 të mbrëmjes dhe orës 8 të mëngjesit, përveç rasteve të urgjencave të reja ose lajmeve shumë të rëndësishme).
- Shpërndani detyrat midis anëtarëve të ekipit dhe kontaktoni palët e treta për mbështetje (analiza, avokim, punë me mediat, dokumentacionin, etj.).
- Kërkoni mbështetje të mëtejshme jashtë "ekipit të krizës" për përmbushjen e nevojave themelore (p.sh. ushqim i rregullt, etj.)

**Masat paraprake të sigurisë digjitale**

Nëse keni arsye të keni frikë nga pasojat për veten tuaj ose mbështetësit e tjerë, përpara se të trajtoni ndonjë emergjencë digjitale të lidhur me arrestimin e mikut tuaj, është gjithashtu e rëndësishme të ndërmerrni disa hapa parandalues në nivelin e sigurisë digjitale për të mbrojtur veten tuaj dhe të tjerët nga rreziku i menjëhershëm.

- Pajtohuni se cilin kanal komunikimi (të sigurt) do të përdorë rrjeti juaj i mbështetësve për të koordinuar zbutjen dhe komunikimin në lidhje me të arrestuarën dhe pasojat e mëtejshme.
- Zvogëloni të dhënat në pajisjet tuaja në minimumin e nevojshëm, dhe mbroni të dhënat në pajisjet tuaja me [enkriptim] (https://ssd.eff.org/en/module/keeping-your-data-safe#1).
- Krijoni [kopje rezervë të sigurt, të enkriptuar](https://communitydocs.accessnow.org/182-Secure_Backup.html) të të gjitha të dhënave tuaja dhe mbajini ato në një vend i cili nuk mund të gjendet gjatë bastisjeve ose arrestimeve të mëtejshme.
- Ndani fjalëkalimet për pajisjet, llogaritë në internet, etj. me një person të besuar i cili nuk është në rrezik direkt.
- Bini dakord për veprimet që duhen ndërmarrë (si pezullimet e llogarive, fshirjen e pajisjes nga distanca, etj.) si reagim i parë ndaj arrestimit tuaj të mundshëm.
- 

## Vlerësimi i rrezikut
### Zvogëloni dëmin e mundshëm të shkaktuar nga veprimet tuaja

Në përgjithësi, duhet të përpiqeni t'i bazoni veprimet tuaja në këtë pyetje:

- Cilat janë ndikimet e veprimeve të vetme dhe të kombinuara mbi të arrestuarën, por edhe mbi komunitetet e tyre, bashkëaktivistët, miqtë, familjen, etj., duke përfshirë edhe veten tuaj?

Secila nga seksionet e mëposhtme do të përshkruajë aspekte të veçanta të këtij vlerësimi të rrezikut.

Konsideratat themelore janë:

- Para se të fshini llogaritë, të dhënat, temat e mediave sociale, etj., sigurohuni që keni dokumentuar përmbajtjen dhe informacionin që e fshini, veçanërisht nëse mund të keni nevojë të riktheni atë përmbajtje ose informacion, ose nëse ju duhet për dëshmi të mëvonshme.
- Nëse i fshini llogaritë ose skedarët (fajllat), kini kujdes sepse:
    - Autoritetet mund ta interpretojnë këtë si shkatërrim ose heqje të provave
    - Kjo mund ta vështirësojë situatën e të arrestuarës, nëse ata do të lejonin qasjen në këto llogari ose skedarë (fajlla), dhe autoritetet nuk mund t'i gjejnë ato, pasi e arrestuara do të paraqitej si jo i besueshëm dhe kjo fshirje mund të shkaktojë marrjen e veprimeve kundër tyre.
- Nëse i informoni njerëzit se informacioni i tyre personal ruhet në një pajisje ose llogari të kapur nga autoritetet, dhe ky komunikim përgjohet, kjo mund të përdoret si provë shtesë sa i përket lidhjes me të arrestuarën.
- Ndryshimet në procedurat e komunikimit (duke përfshirë fshirjen e llogarive, etj.) mund të tërheqin vëmendjen e autoriteteve.

### Informimi i kontakteve

Në përgjithësi, është e pamundur të përcaktohet nëse autoritetet që kanë bërë arrestimin kanë aftësinë për të hartuar rrjetin e kontakteve të të arrestuarës, dhe nëse ata e kanë bërë atë apo jo. Prandaj, ne duhet të supozojmë rastin më të keq, se ata e kanë bërë ose se do ta bëjnë atë.

Para se të filloni të informoni kontaktet e të arrestuarës, ju lutemi vlerësoni rrezikun e informimit të tyre:

- A keni një kopje të listës së kontakteve të të arrestuarës? A mund të kontrolloni se kush është në listën e kontakteve, si në pajisjet e tyre, ashtu edhe në llogaritë e tyre të e-mailit, dhe në platformat e rrjeteve sociale? Hartoni një listë të kontakteve të mundshme për të fituar një pasqyrë se kush mund të preket.
- A ekziston rreziku që informimi i kontakteve të mund t'i lidhë ata më afër me të arrestuarën dhe kjo mund të (keq)përdoret nga autoritetet që kanë bërë arrestimin kundër tyre?
- A duhet të informohen të gjithë, apo vetëm një grup i caktuar njerëzish nga kontaktet?
- Kush do të informojë, edhe atë cilat kontakte? Kush është tashmë në kontakt me kë? Cili është ndikimi i këtij vendimi?
- Vendosni kanalin e komunikimit më të sigurt, duke përfshirë takime personale në hapësira ku nuk ka mbikëqyrje nga kamerat e sigurisë (CCTV) për informimin e kontakteve të implikuara.

### Dokumentoni për të mbajtur evidencë

Para se të fshini ndonjë përmbajtje nga ueb-faqet, faqet e mediave sociale, diskutimet etj., mund të dëshironi të siguroheni që i keni dokumentuar këto më parë. Një arsye për të dokumentuar do të ishte kapja e çdo shenje ose dëshmie të llogarive të abuzuara - si përmbajtje shtesë ose imituese - ose përmbajtje që ju nevojitet si provë ligjore.

Në varësi të ueb-faqes ose platformës së mediave sociale ku dëshironi të dokumentoni burimet ose të dhënat në internet, mund të përdoren qasje të ndryshme:

- Mund të merrni pamje të ekraneve (screenshots) të pjesëve përkatëse (sigurohuni që të dhënat kohore, URL, etj. të jenë të përfshira në pamjet e ekraneve).
- Mund të kontrolloni që ueb-faqet ose blogjet përkatëse janë të indeksuara në [Wayback Machine] (https://archive.org/web), ose të shkarkoni ueb-faqet ose blogjet në kompjuterin tuaj lokal.

*Mos harroni se është e rëndësishme të mbani informacionin që keni shkarkuar në një pajisje të sigurt e cila ruhet në një vend të sigurt.*

### Konfiskimi i pajisjes

Nëse ndonjë pajisje e personit të arrestuar është konfiskuar gjatë ose pas arrestimit, ju lutemi lexoni udhëzuesin [kam humbur pajisjet e mia](../topics/lost-device), në veçanti seksionin për [fshirjen nga distanca](../topics/lost-device/questions/find-erase-device), duke përfshirë rekomandimet në rast të arrestimit.


### Të dhënat dhe llogaritë inkriminuese në internet

Nëse e arrestuara ka informacione në pajisjet e saj që mund ta dëmtojnë atë ose njerëzit e tjerë, mund të jetë ide e mirë të përpiqeni të kufizoni qasjen e të arrestuarës në këtë informacion.

Para se ta bëni këtë, krahasoni rrezikun që paraqet ky informacion me rrezikun e paraqitur nga forcat e sigurisë të cilat mund të shqetësohen për shkak të mungesës së qasjes në këtë informacion (ose ndërmarrjes së veprimeve ligjore për shkak të shkatërrimit të provave). Nëse rreziku që paraqet ky informacion është më i lartë, mund të vazhdoni të hiqni të dhënat përkatëse dhe/ose të mbyllni/pezulloni dhe hequr lidhjen mes llogarive duke i ndjekur udhëzimet më poshtë.

#### Pezullimi ose mbyllja e llogarive në internet

Nëse keni qasje në llogaritë që doni të mbyllni, mund të ndiqni proceset e llogarive të ndryshme. Ju lutemi sigurohuni që të keni një kopje rezervë ose kopje të përmbajtjeve dhe të dhënave të fshira! Jini të vetëdijshëm se pas mbylljes së një llogarie, përmbajtja nuk do të bëhet menjëherë e paarritshme: për shembull, në Facebook, mund të zgjasë deri në dy javë që përmbajtja të fshihet nga të gjithë serverët.

Nëse nuk keni qasje në llogaritë e të arrestuarës ose keni nevojë për veprim më urgjent në llogaritë e mediave sociale, ju lutemi merrni mbështetje nga organizatat e renditura [këtu] (../support) që ofrojnë siguri të llogarisë mes shërbimeve të tyre.

#### Shkëputni lidhjet e llogarive me pajisjet

Ndonjëherë ju gjithashtu mund të dëshironi t'i shkëputni lidhjet e llogarive  me pajisjet, pasi kjo lidhje mund të sigurojë qasje në të dhëna të ndjeshme për këdo që e kontrollon atë pajisje. Për ta bërë këtë, mund të ndiqni [këto udhëzime] (../topics/lost-device/questions/accounts).

Mos harroni të shkëputni lidhjet e [llogarive bankare në internet](#online_bank_accounts) me pajisjet.


#### Ndryshoni fjalëkalimet

Nëse vendosni të mos i mbyllni ose t'i pezulloni llogaritë, mund të jetë megjithatë e dobishme të ndryshoni fjalëkalimet e tyre, duke ndjekur [këto udhëzime] (../topics/lost-device/questions/passwords).

Konsideroni gjithashtu mundësinë e autentifikimit me dy faktorë për të rritur sigurinë e llogarive të të arrestuarës, duke ndjekur [këto udhëzime] (../topics/lost-device/questions/2fa).

Në rast se të njëjtat fjalëkalime janë përdorur në llogari të ndryshme, duhet të ndryshoni të gjitha fjalëkalimet e prekura në ato llogari pasi ato gjithashtu mund të jenë komprometuara.

** Këshilla të tjera për ndryshimin e fjalëkalimeve **

- Përdorni një softuer për menaxhimin e fjalëkalimeve (si [KeepassXC] (https://keepassxc.org)) për të dokumentuar fjalëkalimet e ndryshuara për përdorim të mëtejshëm ose që t'ia dorëzoni të arrestuarës pas lirimit.
- Sigurohuni t'i tregoni të arrestuarës, më së voni pas lirimit të tyre, për fjalëkalimet e ndryshuara dhe kthejuani pronësinë e llogarive.


#### Hiqni anëtarësinë në grupe dhe hiqni dosjet e përbashkëta

Nëse e arrestuara është në ndonjë grup (si për shembull por pa u kufizuar në)  Facebook, WhatsApp, Signal, ose biseda në grup në Wire, ose mund të hyjë në dosje të përbashkëta në internet, dhe prania e saj në këto grupe u jep arrestuesve qasje në informacion të privilegjuar dhe potencialisht të rrezikshëm, ndoshta do të dëshironi të hiqni atë nga grupet ose hapësirat e përbashkëta në internet.

**Udhëzime se si të hiqni anëtarësinë nga grupet e shërbime të ndryshme të porosive të çastit:**

- [WhatsApp](https://faq.whatsapp.com/en/android/26000116/?category=5245251)
- Telegram
    - Në iOS: Personi që e ka krijuar grupin mund t'i largojë pjesëmarrësit duke zgjedhur "Informacione për grupin" (Group info),  dhe duke e lëvizur përdoruesin që duan të largojnë në të majtë.
    - në Android: prekni ikonën e lapsit. Zgjidhni "Anëtarët" (Members) dhe prekni tre pikat pranë emrit të anëtarit që dëshironi ta hiqni dhe zgjidhni "Hiqe nga grupi" (Remove from group).
- Wire
    - [udhëzime për celular](https://support.wire.com/hc/en-us/articles/203526410)
    - [udhëzime për aplikacionin desktop](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- Signal - Nuk mund t'i largoni pjesëmarrësit nga grupet e Signal - ajo që mund të bëni është të krijoni një grup të ri duke e përjashtuar llogarinë e personit të arrestuar.

**Udhëzime se si të largoni persona nga dosjet e përbashkëta në shërbime të ndryshme në internet:**

- [Facebook](https://www.facebook.com/help/211909018842184/)
- GDrive - Së pari kërkoni për skedarët duke përdorur operatorin e kërkimit "to:" (`to:username@gmail.com`), pastaj zgjidhni të gjitha rezultatet dhe klikoni në ikonën "Share". Klikoni "Advanced", hiqni adresën nga dritarja e dialogut dhe ruajeni.
- [Dropbox](https://www.dropbox.com/help/files-folders/unshare-folder#remove-member)
- [iCloud](https://support.apple.com/en-us/HT201081) 


### Fshirja e informacioneve nga profilet

Në disa raste mund të dëshironi ta hiqni përmbajtjen nga grafiku kohor në mediat sociale të të arrestuarës ose nga burimet e tjera që lidhen me llogarinë e tyre, sepse mund të abuzohet si provë kundër tyre ose të krijojë konfuzion dhe konflikt brenda bashkësisë së të arrestuarës ose t’i diskreditojë ata.

Disa shërbime e bëjnë më të lehtë fshirjen e burimeve dhe postimeve nga llogaritë dhe grafiket kohore. Lidhjet për udhëzimet për Twitter, Facebook dhe Instagram janë dhënë më poshtë. Ju lutemi sigurohuni që keni dokumentuar përmbajtjen që dëshironi të fshini në rast se mund të keni nevojë përsëri për të si dëshmi në rast të ndërhyrjes etj..

- Për Twitter, mund të përdorni [Tweet Deleter](https://tweetdeleter.com/).
- Për Facebook, mund të ndiqni [këtë udhëzues](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), bazuar në një aplikacion për shfletuesin Chrome të quajtur [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- Për Instagram, mund të ndiqni [këto udhëzime] (https://help.instagram.com/997924900322403) (por kini kujdes që njerëzit me qasje në përzgjedhjet e llogarisë tuaj do të jenë në gjendje të shohin historinë e shumicës së ndërveprimeve tuaja edhe pas fshirjes së përmbajtjes).


### Fshini shoqërimet e dëmshme në internet

Nëse ka ndonjë informacion në internet ku përmendet emri i të arrestuarës që mund të ketë pasoja negative për të ose kontaktet e saj, është ide e mirë ta fshini nëse kjo nuk do të dëmtojë më tej të arrestuarën.

- Bëni një listë të hapësirave dhe informacioneve në internet që duhet të hiqen ose të ndryshohen.
- Nëse keni identifikuar përmbajtjen që duhet të hiqet ose të ndryshohet, mund të dëshironi që të bëni kopje rezervë përpara se të vazhdoni me fshirjen e saj ose me kërkesat për heqjen e saj.
- Vlerësoni nëse heqja e emrit të të arrestuarës mund të ketë ndikim negativ në situatën e tyre (p.sh. heqja e emrave të tyre nga lista e punonjësve të një organizate mund ta mbrojë organizatën, por gjithashtu mund ta heqë arsyetimin për të arrestuarën, për shembull se ata kanë punuar për atë organizatë).
- Nëse keni qasje në ueb-faqet ose llogaritë përkatëse, ndryshoni ose hiqni përmbajtjen dhe informacionin e ndjeshëm.
 - Nëse nuk keni qasje, kërkojuni njerëzve që kanë qasje të heqin informacionin e ndjeshëm.
- Udhëzime për heqjen e përmbajtjes në shërbimet e Google mund të gjeni [këtu](https://support.google.com/webmasters/answer/6332384?hl=en#get_info_off_web)
- Kontrolloni nëse ueb-faqet që përmbajnë informacione janë indeksuar nga Wayback Machine ose Google Cache. Nëse po, edhe kjo përmbajtje duhet të hiqet.

<a name="online_bank_accounts"></a>
### Llogaritë bankare në internet

Shumë shpesh, llogaritë bankare menaxhohen dhe mund të qasen në internet, dhe verifikimi përmes pajisjeve mobile mund të jetë i nevojshëm për transaksione ose madje edhe mënyra e vetme për të hyrë në llogarinë online. Nëse e arrestuara nuk mund të hyjë në llogaritë e saj bankare për një periudhë më të gjatë, kjo mund të ketë pasoja për gjendjen financiare të të arrestuarës dhe aftësinë e tyre për të hyrë në llogari. Në këto raste, sigurohuni që keni:

- Shkëputur pajisjet e sekuestruara nga llogaria/të bankare e të arrestuarës.
- Merrni autorizim dhe prokurë nga e arrestuara për të qenë në gjendje të administroni llogarinë bankare në emrin e tyre që në fillim të procesit (në marrëveshje me të afërmit e tyre).


## Këshillat përfundimtare

- Sigurohuni që t'ia ktheni të gjithë pronësinë e të dhënave të arrestuarës pas lirimit të saj
- Lexoni [këto këshilla](../topics/lost-device/questions/device-returned) se si t'i trajtoni pajisjet e sekuestruara pasi ato të kthehen nga autoritetet. 

