---
layout: page
title: "Pajisja ime reagon në mënyrë të dyshimtë"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: sq
summary: "Nëse kompjuteri ose telefoni juaj reagon në mënyrë të dyshimtë, mund të ketë softuer të padëshiruar ose dashakeq në pajisjen tuaj."
date: 2019-03
permalink: /sq/topics/device-acting-suspiciously
parent: Home
---

# Pajisja ime reagon në mënyrë të dyshimtë

Sulmet malware kanë evoluar dhe janë bërë shumë të sofistikuara me kalimin e viteve. Këto sulme paraqesin kërcënime të ndryshme dhe mund të kenë pasoja serioze në infrastrukturën dhe të dhënat tuaja personale dhe të organizatës.

Sulmet malware vijnë në forma të ndryshme, të tilla si viruse, phishing, ransomware, trojanë dhe rootkits. Disa nga kërcënimet janë: ndalja e papritur e kompjuterëve, vjedhja e të dhënave (gjegjësisht.: kredencialet e llogarive të ndjeshme, informacione financiare, të dhëna për hyrje në llogari bankare), një sulmues që ju shantazhon  për të paguar një shpërblim duke marrë kontrollin e pajisjes tuaj, ose duke marrë kontrollin e pajisjes tuaj dhe duke e përdorur atë për të filluar sulme DDoS.

Disa metoda që përdoren zakonisht nga sulmuesit për t'u komprometuar juve dhe pajisjet tuaja duken si aktivitete të rregullta, siç janë:

- Një e-mail ose një postim në mediat sociale që do t'ju joshin të hapni një dokument të bashkangjitur ose të klikoni në një lidhje.

- Shtyrja e njerëzve që të shkarkojnë dhe të instalojnë softuer nga një burim i pabesueshëm.

- Shtytja e dikujt për të futur emrin e përdoruesit dhe fjalëkalimin e tij në një ueb-faqe që është bërë për t'u dukur e legjitime, por nuk është e tillë.

Kjo pjesë e Veglërisë së ndihmës së parë digjitale do t'ju njoftojë me disa hapa themelorë për të kuptuar nëse pajisja juaj ka të ngjarë të jetë e infektuar apo jo.

Nëse mendoni se kompjuteri juaj ose pajisja celulare ka filluar të reagojë në mënyrë të dyshimtë, së pari duhet të mendoni se cilat janë simptomat.

Simptomat që zakonisht mund të interpretohen si aktivitet i dyshimtë i pajisjes, por shpesh nuk janë arsye e mjaftueshme për t'u shqetësuar përfshijnë:

- Zhurma të klikimit gjatë thirrjeve telefonike
- Zbrazja e papritur e baterisë
- Mbinxehja përderisa pajisja nuk është në përdorim
- Pajisja funksionon ngadalë

Këto simptoma shpesh keqkuptohen si tregues të besueshëm të veprimtarisë shqetësuese të pajisjes. Megjithatë, secila prej tyre e marrë vetë nuk është arsye e mjaftueshme për shqetësim.

Simptomat e besueshme të një pajisje të komprometuar zakonisht janë:

- Pajisja riniset (ristartohet) shpesh vetvetiu
- Aplikacionet ndërpriten aksidentalisht, veçanërisht pas veprimit të futjes së të dhënave
- Përditësimet e sistemit operativ dhe/ose korrigjuesit e sigurisë dështojnë në mënyrë të përsëritur
- Drita e treguesit të aktivitetit të kamerës është ndezur ndërsa kamera nuk është në përdorim
- Mesazhe të përsëritura ["Blue Screens of Death"](https://en.wikipedia.org/wiki/Blue_Screen_of_Death) (ekranet e kaltra të vdekjes) ose kernel panics (panik e bërthamës).
- Dritare vezulluese
- Paralajmërimet e antivirusit

## Workflow

### start

Duke pasur parasysh informacionin e dhënë në hyrje, nëse ende mendoni se pajisja juaj mund të jetë komprometuar, udhëzuesi i mëposhtëm mund t'ju ndihmojë ta identifikoni problemin.

 - [Besoj se pajisja ime celulare reagon në mënyrë të dyshimtë](#phone-intro)
 - [Besoj se kompjuteri im reagon në mënyrë të dyshimtë](#computer-intro)
 - [Nuk mendoj më se pajisja ime mund të jetë komprometuar](#device-clean)


### device-clean

> Shumë mirë! Sidoqoftë, mbani në mend se këto udhëzime do t'ju ndihmojnë të bëni vetëm një analizë të shpejtë. Megjithëse duhet të jetë e mjaftueshme për të identifikuar anomali të dukshme, softuerë më të sofistikuar për spiunim (spyware) mund të jenë të aftë të fshihen në mënyrë më efektive.

Nëse ende dyshoni se pajisja mund të jetë komprometuar, mund të:

- [kërkoni ndihmë shtesë](#malware_end)
- [vazhdoni drejtpërdrejt me resetimin e pajisjes] (#reset).


### phone-intro

> Është e rëndësishme të mendoni se si pajisja juaj mund të jetë komprometuar.
>
> - Sa kohë më parë filluat të dyshonit se pajisja juaj po reagonte në mënyrë të dyshimtë?
> - A ju kujtohet të keni klikuar në ndonjë lidhje nga burime të panjohura?
> - A keni marrë mesazhe nga palë që nuk i njihni?
> - A keni instaluar ndonjë softuer të shkarkuar nga burime jo të besueshme?
> - A e ka përdorë pajisjen dikush tjetër?

Mendoni për këto pyetje me qëllim që të përpiqeni t'i identifikoni rrethanat, nëse ka, që çuan në komprometimin e pajisjes tuaj.

 - [Kam pajisje Android](#android-intro)
 - [Kam pajisje iOS](#ios-intro)


### android-intro

> Fillimisht kontrolloni nëse ka ndonjë aplikacion të panjohur të instaluar në pajisjen tuaj Android.
>
> Mund të gjeni një listë të aplikacioneve në seksionin "Aplikacionet" të menysë së cilësimeve. Identifikoni çdo aplikacion që nuk ka qenë i para-instaluar në pajisjen tuaj dhe që nuk mbani mend se e keni shkarkuar.
>
> Nëse dyshoni në ndonjërin prej aplikacioneve në listë, bëni një kërkim në internet dhe shihni burimet për të parë nëse ka ndonjë raport të besueshëm që e identifikojnë aplikacionin si keqdashës.

A gjetët ndonjë aplikacion të dyshimtë?

 - [Jo, nuk gjeta](#android-unsafe-settings)
 - [Po, kam identifikuar aplikacione potencialisht keqdashëse](#android-badend)


### android-unsafe-settings

> Android u jep përdoruesve mundësinë që të kenë qasje të nivelit më të ulët në pajisjen e tyre. Kjo mund të jetë e dobishme për zhvilluesit e programeve kompjuterike, por gjithashtu mund t’i ekspozojë pajisjet ndaj sulmeve shtesë. Ju duhet t’i rishikoni këto cilësime të sigurisë dhe të siguroheni se ato janë vendosur në opsionet më të sigurta. Prodhuesit mund të dërgojnë pajisje me cilësime standarde të pasigurta. Këto cilësime duhet të rishikohen edhe nëse nuk keni bërë ndryshime vetë.
>
> #### Aplikacione nga burime jo të besueshme
>
> Android-i normalisht e bllokon instalimin e aplikacioneve që nuk janë të ngarkuar nga Google Play Store. Google ka procese për të rishikuar dhe identifikuar aplikacione keqdashëse në Play Store. Sulmuesit shpesh përpiqen t’i shmangin këto kontrolle duke u ofruar aplikacione keqdashëse direkt përdoruesve, duke e ndarë me ta një lidhje ose një skedar. Është e rëndësishme të sigurohet që pajisja juaj të mos lejojë instalimin e aplikacioneve nga burime jo të besueshme.
>
> Sigurohuni që programet për instalim nga burime të panjohura janë të çaktivizuara në pajisjen tuaj. Në shumë versione të Android-it, këtë mundësi do ta gjeni në cilësimet e sigurisë, por mund të jetë diku tjetër, në varësi të versionit të Android-it që jeni duke e përdorur, ndërsa në telefonat më të rinj kjo mundësi mund të jetë në cilësimet e lejeve për secilin aplikacion.
>
> #### Modaliteti i zhvilluesit të aplikacioneve
>
> Android u lejon zhvilluesve të aplikacioneve t’i ekzekutojnë drejtpërdrejt komandat në sistemin themelor operativ, ndërsa gjenden në "Modalitetin e zhvilluesit të aplikacioneve". Kur aktivizohet, kjo i ekspozon pajisjet në sulme fizike. Dikush me qasje fizike në pajisje mund të përdorë Modalitetin e zhvilluesit të aplikacioneve për të shkarkuar kopje të të dhënave private nga pajisja ose për të instaluar aplikacione keqdashëse.
>
> Nëse shihni një meny të Modalitetit të zhvilluesit të aplikacioneve në cilësimet e pajisjes tuaj, atëherë duhet të siguroheni se është çaktivizuar.
>
> #### Google Play Protect
>
> Shërbimi Google Play Protect është i disponueshëm në të gjitha pajisjet bashkëkohore Android. Kryen skanime të rregullta të të gjitha aplikacioneve të instaluara në pajisjen tuaj. Play Protect gjithashtu mund të heqë automatikisht çdo aplikacion të njohur keqdashës nga pajisja juaj. Mundësimi i këtij shërbimi dërgon informacione në lidhje me pajisjen tuaj (për aplikacione të tilla të instaluara) në Google.
>
> Google Play Protect mund të aktivizohet nën cilësimet e sigurisë së pajisjes tuaj. Më shumë informacione ka në ueb-faqen [Play Protect](https://www.android.com/play-protect/).

A identifikuat ndonjë cilësim të pasigurt?

- [Jo, nuk gjeta](#android-bootloader)
- [Po, identifikova cilësime potencialisht të pasigurta](#android-badend)


### android-bootloader

> Android Bootloader është një program kyç që aktivizohet sapo ta ndizni pajisjen tuaj. Bootloader-i i mundëson sistemit operativ të fillojë dhe ta përdorë pajisjen. Një Bootloader i komprometuar i jep sulmuesit qasje të plotë në harduerin e pajisjes. Shumica e prodhuesve i dërgojnë pajisjet e tyre me Bootloader të mbyllur. Një mënyrë e zakonshme për të identifikuar nëse Bootloader zyrtar i prodhuesit është ndryshuar është të rindezni pajisjen tuaj dhe ta shihni logon e Bootloaderit. Nëse shfaqet një trekëndësh i verdhë me një shenjë pikëpyetjeje, atëherë Bootloader-i origjinal është zëvendësuar. Pajisja juaj gjithashtu mund të jetë komprometuar nëse tregon një pamje paralajmëruese Bootloader-it të zhbllokuar, e ju nuk e keni zhbllokuar atë vetë për të instaluar një Android ROM të përdoruesit si CyanogenMod. Duhet ta resetoni pajisjen tuaj në cilësimet e fabrikës nëse Bootloader-i tregon pamje paralajmëruese të zhbllokimit që nuk e prisni.

A është komprometuar Bootloader-i apo pajisja juaj po e përdor Bootloader-in origjinal?

- [Bootloader-i i pajisjes time është i komprometuar](#android-badend)
- [Pajisja ime është duke përdorur Bootloader-in origjinal](#android-goodend)


### android-goodend

> Me sa duket pajisja juaj nuk është komprometuar.

A jeni ende të shqetësuar se pajisja juaj është e komprometuar?

- [Po, do të dëshiroja të kërkoja ndihmë profesionale](#malware_end)
- [Jo, i kam zgjidhur problemet e mia](#resolved_end)


### android-badend

> Pajisja juaj mund të jetë komprometuar. [Resetimi] (#reset) në cilësimet e fabrikës ka të ngjarë të largojë çdo kërcënim që është i pranishëm në pajisjen tuaj. Sidoqoftë, nuk është gjithmonë zgjidhja më e mirë. Përveç kësaj, mund të dëshironi të hetoni çështjen më tej për të identifikuar nivelin e ekspozimit dhe natyrën e saktë të sulmit që keni përjetuar.
>
> Mund të dëshironi të përdorni një mjet për vetë-diagnostikim të quajtur [Emergency VPN] (https://www.civilsphereproject.org/emergence-vpn), ose të kërkoni ndihmë nga ndonjë organizatë që mund të ndihmojë.

Dëshironi të kërkoni ndihmë të mëtejshme?

- [Po, do të dëshiroja të kërkoja ndihmë profesionale](#malware_end)
- [Jo, kam një rrjet lokal mbështetës të cilin mund ta kontaktoj](#resolved_end)


### ios-intro

> Kontrolloni cilësimet e iOS për të parë nëse ka ndonjë gjë të pazakontë.
>
> Në aplikacionin Cilësimet (Settings), kontrolloni që pajisja juaj të jetë e lidhur me Apple ID tuaj. Artikulli i parë i menysë në të majtë duhet të jetë emri juaj ose emri që përdorni për Apple ID tuaj. Klikoni mbi të dhe kontrolloni se a paraqitet adresa e saktë e e-mail-it. Në fund të kësaj faqeje do të shihni një listë me emrat dhe modelet e të gjitha pajisjeve iOS të lidhura me këtë Apple ID.

 - [Të gjitha informacionet janë të sakta dhe unë kam ende nën kontroll Apple ID time](#ios-goodend)
 - [Emri ose detajet e tjera janë të pasakta ose shoh pajisje në listë që nuk janë të miat](#ios-badend)


### ios-goodend

> Nuk duket që pajisja juaj është komprometuar.

A jeni ende të shqetësuar se pajisja juaj është e komprometuar?

- [Po, do të dëshiroja të kërkoja ndihmë profesionale](#malware_end)
- [Jo, i kam zgjidhur problemet e mia](#resolved_end)


### ios-badend

> Pajisja juaj mund të jetë komprometuar. Resetimi në cilësimet e fabrikës ka të ngjarë të largojë çdo kërcënim që është i pranishëm në pajisjen tuaj. Sidoqoftë, kjo nuk është gjithmonë zgjidhja më e mirë. Përveç kësaj, ju mund të dëshironi të hetoni çështjen më tej për të identifikuar nivelin tuaj të ekspozimit dhe natyrën e saktë të sulmit që keni përjetuar.
>
> Mund të dëshironi të përdorni një mjet për vetë-diagnostikim të quajtur [Emergency VPN] (https://www.civilsphereproject.org/emergence-vpn), ose të kërkoni ndihmë nga ndonjë organizatë që mund të ndihmojë.

Dëshironi të kërkoni ndihmë të mëtejshme?

- [Po, do të dëshiroja të kërkoja ndihmë profesionale](#malware_end)
- [Jo, kam një rrjet lokal mbështetës të cilin mund ta kontaktoj](#resolved_end)


### computer-intro

> **Shënim: Në rast se jeni duke u sulmuar nga softuer që kërkon të paguani shpengim, shkoni drejtpërdrejt te [kjo ueb-faqe](https://www.nomoreransom.org/).**
>
> Kjo rrjedhë pune do t'ju ndihmojë të hetoni aktivitetet e dyshimta në pajisjen tuaj kompjuterike. Nëse i ndihmoni ndonjë personi nga largësia, mund të përpiqeni të ndiqni hapat e përshkruar në lidhjet e mëposhtme duke përdorur një softuer desktopi për punë nga largësia si TeamViewer, ose mund të shikoni softuerin forenzik nga largësia siç është [Google Rapid Response (GRR)] (https: // github.com/google/grr). Mbani në mend se vonesa dhe besueshmëria e rrjetit do të jenë thelbësore për ta bërë këtë si duhet.

Ju lutemi zgjidhni sistemin operativ që dëshironi ta kontrolloni:

 - [Windows](#windows-intro)
 - [macOS](#mac-intro)


### windows-intro

> Mund të ndiqni këtë udhëzues hyrës për të hetuar aktivitetet e dyshimta në pajisjet me Windows:
>
> - [How to Live Forensic on Windows by Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

A ju ndihmuan këto udhëzime për të identifikuar ndonjë aktivitet keqdashës?

 - [Po, mendoj se kompjuteri është i infektuar](#device-infected)
 - [Jo, nuk u identifikua asnjë aktivitet keqdashës](#device-clean)


### mac-intro

> Për të identifikuar një infektim të mundshëm në kompjuter Mac, duhet t'i ndiqni këto hapa:
>
> 1. Kontrolloni se a ka programe të dyshimta që fillojnë automatikisht
> 2. Kontrolloni se a ka procese aktive të dyshimta
> 3. Kontrolloni se a ka zgjerime të dyshimta të bërthamës (kernelit)
>
> Ueb-faqja [Objective-See](https://objective-see.com) ofron disa shërbime falas që e lehtësojnë këtë proces:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) mund të përdoret për të identifikuar të gjitha programet që janë regjistruar për të filluar automatikisht.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) mund të përdoret për të kontrolluar proceset që ekzekutohen dhe për t'i identifikuar ato që duken të dyshimta (për shembull sepse nuk janë nënshkruar, ose për shkak se ato janë shënuar nga VirusTotal).
> - [KextViewr](https://objective-see.com/products/kextviewr.html) mund të përdoret për të identifikuar çdo zgjerim të dyshimtë të bërthamës (kernelit) që është ngarkuar në kompjuterin Mac.
>
> Në rast se këto nuk zbulojnë menjëherë ndonjë gjë të dyshimtë dhe dëshironi të kryeni kontrolle të mëtejshme, mund të përdorni [Snoopdigg] (https://github.com/botherder/snoopdigg). Snoopdigg është një mjet që thjeshton procesin e mbledhjes së disa informacioneve në sistem dhe ofron një pasqyrë të plotë të memories.
>
> Një mjet shtesë që mund të jetë i dobishëm për të mbledhur detaje të mëtejshme (por që kërkon disa njohuri me komandat e terminalit) është [AutoMacTC] (https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) nga kompania amerikane për siguri në internet, CrowdStrike.

A ju ndihmuan këto udhëzime për të identifikuar ndonjë aktivitet keqdashës?

 - [Po, mendoj se kompjuteri është i infektuar](#device-infected)
 - [Jo, nuk u identifikua asnjë aktivitet keqdashës](#device-clean)

### device-infected

O jo! Për të hequr qafe infeksionin mund të:

- [kërkoni ndihmë shtesë](#malware_end)
- [vazhdoni drejtpërdrejt me resetimin e pajisjes] (#reset).

### reset

> Mund të konsideroni resetimin e pajisjes tuaj si një masë shtesë e kujdesit. Udhëzuesit e mëposhtëm do të japin udhëzime të përshtatshme për llojin tuaj të pajisjes:
>
> - [Android](https://www.howtogeek.com/248127/how-to-wipe-your-android-device-and-restore-it-to-factory-settings/)
> - [iOS](https://support.apple.com/en-us/HT201252)
> - [Windows](https://support.microsoft.com/en-us/help/4000735/windows-10-reinstall)
> - [Mac](https://support.apple.com/en-us/HT201314)

A mendoni se keni nevojë për ndihmë shtesë?

- [Po](#malware_end)
- [Jo](#resolved_end)


### malware_end

Nëse keni nevojë për ndihmë shtesë për t'u marrë me një pajisje të infektuar, mund t'i drejtoheni organizatave të renditura më poshtë.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Shpresojmë që DFAK ishte i dobishëm. Ju lutemi na jepni komentet tuaja (përmes e-mailit) (mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Këtu janë disa këshilla që të mos lejoni të bini pre e përpjekjeve të sulmuesve për të komprometuar pajisjet dhe të dhënat tuaja:

- Gjithmonë kontrolloni dy herë legjitimitetin e çdo e-maili që merrni, e skedarit që keni shkarkuar ose të lidhjes që ju pyet për detajet e hyrjes në llogarinë tuaj.
- Lexoni më shumë rreth asaj se si ta mbroni pajisjen tuaj nga infeksionet me softuer keqdashës në udhëzuesit lidhjet e të cilave janë dhënë te burimet.

#### Resources

- [Siguria në një kuti  - Mbroni pajisjen tuaj nga sulmet e softuerit keqdashës dhe vjedhja e të dhënave të ndjeshme (phishing).](https://securityinabox.org/en/guide/malware/)
