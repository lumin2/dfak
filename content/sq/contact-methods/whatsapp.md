---
layout: page
title: WhatsApp
author: mfc
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/whatsapp.md
parent: /sq/
published: true
---

Përdorimi i WhatsApp-it do të sigurojë që biseda juaj me marrësin të mbrohet në mënyrë që vetëm ju dhe marrësi të lexoni komunikimet, megjithatë fakti që keni komunikuar me marrësin mund të jetë i arritshëm nga qeveritë ose organet e zbatimit të ligjit.

Burime: [How to: Use WhatsApp on Android](https://ssd.eff.org/en/module/how-use-whatsapp-android) [How to: Use WhatsApp on iOS](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
