---
layout: page
title: Numri i telefonit
author: mfc
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/phone-number.md
parent: /sq/
published: true
---

Komunikimet telefonike celulare dhe fikse nuk janë të enkriptuara për marrësit tuaj, kështu që përmbajtja e bisedës suaj dhe informacioni se me kë bisedoni është i arritshëm nga qeveritë, organet e zbatimit të ligjit ose palë të tjera me pajisjet e nevojshme teknike.
