---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: lunes-jueves 9am-5pm CET
response_time: 4 días
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

El objetivo de la Asociación de Defensores Digitales (DDP por sus siglas en Inglés) es proteger y promover la libertad en Internet, y mantenerlo libre de amenazas emergentes, especialmente en entornos represivos. Coordinamos soporte de emergencia para individuos y organizaciones como por ejemplo defensores de derechos humanos, periodistas, activistas de la sociedad civil y blogueros. Adoptamos una visión centrada en las personas y nos enfocamos en nuestros valores fundamentales de transparencia, derechos humanos, inclusión y diversidad, igualdad, confidencialidad y libertad. DDP se formó en 2012 por la Coalición de Libertad Online (FOC por sus siglas en Inglés).

El DDP tiene tres tipos diferentes de financiamiento dirigidas a situaciones de emergencia, así como subvenciones a largo plazo centradas en el desarrollo de capacidades dentro de una organización. Además, coordinamos una beca de integridad digital donde las organizaciones reciben capacitación personalizada en seguridad y privacidad digital, y un programa de red de respuesta rápida.
