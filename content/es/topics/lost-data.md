---
layout: page
title: "Perdí mi información"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: es
summary: "Qué hacer en caso de que pierdas datos"
date: 2019-8
permalink: /es/topics/lost-data
parent: /es/
---

# Perdí mi información

Los datos digitales pueden ser muy efímeros e inestables, y existen muchas formas de perderlos. El daño físico de tus dispositivos, la cancelación de tus cuentas, información borrada por error, las actualizaciones de software y los fallos en programas pueden provocar pérdida de datos. Además, a veces es posible que no sepas cómo funciona tu sistema de respaldos, o simplemente te hayas olvidado de tus credenciales o la ruta para encontrar o recuperar dichos datos.

Esta sección del kit de primeros auxilios digitales te guiará a través de algunos pasos básicos para diagnosticar cómo podrías haber perdido los datos y las posibles estrategias de mitigación para recuperarlos.

A continuación, un cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### entry_point

> En esta sección nos centramos principalmente en datos basados en dispositivos. Para contenidos y credenciales en línea, te enviaremos a otras secciones del Kit de primeros auxilios digitales.
>
> Estos dispositivos incluyen computadoras, dispositivos móviles, discos duros externos, memorias USB y tarjetas SD.

¿Qué tipo de datos perdiste?

- [Contenido en línea](../../../account-access-issues)
- [Credenciales](../../../account-access-issues)
- [Contenido en dispositivos](#content_on_device)

### content_on_device

> A menudo, los datos "perdidos" representan información que simplemente se ha eliminado, ya sea accidental o intencionalmente. En tu computadora, revisa tu Papelera de reciclaje. En dispositivos Android, puedes encontrar los datos perdidos en el directorio LOST.DIR. Si los archivos perdidos no se pueden ubicar en la Papelera de reciclaje de tu sistema operativo, es posible que no se hayan eliminado, pero que estén en una ubicación diferente a la que esperabas. Prueba la función de búsqueda en tu sistema operativo para localizarlos.
>
> Verifica también los archivos y carpetas ocultos, ya que los datos que has perdido pueden estar allí. Así puedes hacerlo en [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) y [Windows](https://support.microsoft.com/en-ie/help/4028316/windows-view-hidden-files-and-folders-in-windows-10). Para Linux, ve a tu directorio de inicio en un administrador de archivos y pulsa Ctrl + H. Esta opción está disponible también en Android y iOS.
>
> Intenta buscar el nombre exacto de tu archivo perdido. Si esto no funciona, o si no estás seguro del nombre exacto, puedes intentar una búsqueda con comodines (es decir, si perdiste un archivo docx, pero no estás seguro del nombre, puedes buscar `*.docx`) que solo mostrará archivos con la extensión `.docx`. Ordena por *Fecha de modificación* para localizar rápidamente los archivos más recientes al usar la opción de búsqueda con comodín.
>
> Además de buscar los datos perdidos en tu dispositivo, pregúntate si has enviado un correo electrónico o lo has compartido con alguien (tu mismo incluido) o lo has agregado a tu almacenamiento en la nube en cualquier momento. Si ese es el caso, puedes realizar una búsqueda allí y es posible que puedas recuperar alguna versión del mismo.
>
> Para aumentar tus posibilidades de recuperar datos, deja de usar tus dispositivos inmediatamente. La escritura continua en el disco duro puede disminuir las posibilidades de encontrar/restaurar datos. Piensa en ello como intentar recuperar un mensaje escrito en un bloc de notas: cuanto más escriba en las páginas del bloc de notas, menos posibilidades tendrá de encontrar lo que estás buscando.

¿Cómo perdiste tus datos?

- [El dispositivo en donde se almacenaron los datos sufrió daño físico](#tech_assistance_end)
- [El dispositivo en donde se almacenaron los datos fue robado/perdido](#device_lost_theft_end)
- [Los datos fueron eliminados](#where_is_data)
- [Los datos desaparecieron después de una actualización de software](#software_update)
- [El sistema o un programa se bloqueó y los datos desaparecieron](#where_is_data)


### software_update

> Algunas veces, cuando actualizas un programa o el sistema operativo completo, pueden ocurrir problemas inesperados, lo que hace que el sistema o el programa deje de funcionar como debería, o que ocurra alguna falla, incluyendo la corrupción de datos. Si notaste una pérdida de datos justo después de una actualización de software o del sistema operativo, vale la pena considerar restaurarlo a un estado anterior. Las restauraciones son útiles porque significan que tu software o bases de datos puede ser revertidas a un estado limpio y consistente, incluso después de que hayan ocurrido operaciones erróneas o bloqueos de software.
>
> El método para restaurar una pieza de software a una versión anterior depende de cómo está construido ese software: en algunos casos es posible de forma sencilla, en otros no, y en algunos se necesita algo de trabajo. Por lo tanto, tendrás que hacer una búsqueda en línea para volver a versiones anteriores. Ten en cuenta que restaurar también se conoce como "desactualizar" (downgrading en Inglés), así que puedes hacer una búsqueda con este término también. Para sistemas operativos como Windows o Mac, cada uno tiene sus propios métodos para restaurar a versiones anteriores.
>
> - Para sistemas Mac, visita la [base de conocimientos del Centro de soporte de Apple](https://support.apple.com/es-lamr) y usa los terminos "volver a" con tu versión macOS para leer más y encontrar instrucciones.
> - Para sistemas Windows, el procedimiento varía dependiendo de si deseas deshacer una actualización específica o restaurar todo el sistema. En ambos casos, puede encontrar instrucciones en el [Centro de soporte de Microsoft](https://support.microsoft.com/es-us); por ejemplo, para Windows 10 puedes buscar la pregunta "¿Cómo elimino una actualización instalada?" En esta página de [preguntas frecuentes de Windows Update](https://support.microsoft.com/es-us/help/12373/windows-update-faq).

¿Fue útil la restauración de software?

- [Sí](#resolved_end)
- [No](#where_is_data)


### where_is_data

> La copia de seguridad de los datos hecha de forma regular es una práctica recomendada. En ocasiones, olvidamos que tenemos la copia de seguridad automática habilitada, por lo que vale la pena revisar si tu dispositivo la tenía, y usar esta copia de seguridad para restaurar tus datos. En caso de que no, te recomendamos la planificación de futuras copias de seguridad, en las [sugerencias finales de este flujo de trabajo](#resolved_end).

Para verificar si tienes una copia de seguridad de los datos que perdiste, comienza por preguntarte en dónde estaban almacenados los datos perdidos.

- [En dispositivos de almacenamiento (disco duro externo, memorias USB, tarjetas SD)](#storage_devices_end)
- [En una computadora](#computer)
- [En un dispositivo móvil](#mobile)

### computer

¿Qué tipo de sistema operativo se ejecuta en tu computadora?

- [MacOS](#macos_computer)
- [Windows](#windows_computer)
- [Gnu/Linux](#linux_computer)

### macos_computer

> Para verificar si tu dispositivo MacOS tenía la opción de copia de seguridad habilitada y usarla para restaurar tus datos, verifica tu [iCloud](https://support.apple.com/es-lamr/HT208682) o [Time Machine](https://support.apple.com/es-lamr/HT201250)
>
> Un lugar para chequear es la lista de elementos recientes, que hace un seguimiento de las aplicaciones, archivos y servidores que has utilizado durante tus últimas sesiones en la computadora. Para buscar el archivo y volver a abrirlo, ve al Menú Apple en la esquina superior izquierda, selecciona Elementos recientes y busca la lista de archivos. Más detalles se pueden encontrar [aquí - En Inglés](https://www.nytimes.com/2018/08/01/technology/personaltech/mac-find-lost-files.html).

¿Pudiste localizar tus datos o restaurarlos?

- [Sí](#resolved_end)
- [No](#tech_assistance_end)

### windows_computer

> Para comprobar si tienes una copia de seguridad habilitada en tu computadora con Windows, puedes leer [estas instrucciones](https://support.microsoft.com/es-us/help/4027408/windows-10-backup-and-restore).
>
> Windows 10 incluye la característica **Línea de tiempo**, que está diseñada para mejorar tu productividad al almacenar un registro de los archivos que utilizaste, los sitios que exploraste y otras acciones que realizaste en tu computadora. Si no puedes recordar dónde guardaste un documento, puedes hacer clic en el ícono Línea de tiempo en la barra de tareas de Windows 10 para ver un registro visual organizado por fecha y volver a lo que necesites haciendo clic en el ícono de vista previa correspondiente. Esto podría ayudarte por ejemplo a localizar un archivo cuyo nombre ha sido cambiado. Si la Línea de tiempo está habilitada, parte de la actividad de tu PC -como los archivos que editaste en Microsoft Office - también se pueden estar sincronizando con tu dispositivo móvil u otra computadora que hayas usado, por lo que es posible que tengas una copia de seguridad de los datos perdidos en otros dispositivos. Puedes leer más sobre Línea de tiempo y cómo usarla, [aquí](https://support.microsoft.com/es-us/help/4230676/windows-10-get-help-with-timeline) o [aquí - En Inglés](https://www.nytimes.com/2018/07/05/technology/personaltech/windows-10-timeline.html).

¿Pudiste localizar tus datos o restaurarlos?

- [Sí](#resolved_end)
- [No](#windows_restore)

### windows_restore

> En algunos casos, existen herramientas gratuitas y de código abierto que pueden ser útiles para encontrar contenido faltante y recuperarlo. A veces, tienen un uso limitado y la mayoría de las herramientas conocidas cuestan algo de dinero.
>
> Por ejemplo, [Recuva](http://www.ccleaner.com/recuva) es un programa gratuito conocido para la recuperación de datos. Recomendamos darle una oportunidad y ver cómo te va.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech_assistance_end)


### linux_computer

> Algunas de las distribuciones de Linux más populares, como Ubuntu, tienen una herramienta de copia de seguridad integrada, por ejemplo [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) en Ubuntu. Si se incluye una herramienta de respaldo integrada en tu sistema operativo, es posible que se te haya solicitado que habilites los respaldos automáticos cuando comenzaste a usar la computadora por primera vez. Busca en tu distribución de Linux para ver si esta incluye una herramienta de copia de seguridad, y cuál es el procedimiento para verificar si está habilitada y restaurar dichos datos.
>
> Para Déja Dup en Ubuntu, puedes consultar [este artículo - (En Inglés)](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) para verificar si tienes respaldos automáticos habilitados en su máquina. y en [esta página (En Inglés)](https://wiki.gnome.org/DejaDup/Help/Restore/Full) para obtener instrucciones sobre cómo restaurar los datos perdidos de una copia de seguridad existente.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech_assistance_end)

### mobile

¿Qué sistema operativo se ejecuta en tu móvil?

- [iOS](#ios_phone)
- [Android](#android_phone)


### ios_phone

> Es posible que hayas habilitado una copia de seguridad automática con iCloud o iTunes. Lee [esta guía](https://support.apple.com/kb/PH12521?locale=en_US&viewlocale=es_ES) para verificar si tienes alguna copia de seguridad existente y para aprender cómo restaurar tus datos.

¿Has encontrado tu copia de seguridad y has recuperado tus datos?

- [Sí](#resolved_end)
- [No](#phone_which_data)

###  phone_which_data

¿Cuál de las siguientes opciones se aplica a los datos que perdiste?

- [Son datos generados por aplicaciones, por ejemplo contactos, actualizaciones de contenidos, etc.](#app_data_phone)
- [Son datos generado por el usuario, por ejemplo Fotos, videos, audio, notas](#ugd_phone)

### app_data_phone

> Una aplicación móvil es una aplicación de software diseñada para ejecutarse en tu dispositivo móvil. Sin embargo, a la mayoría de estas aplicaciones también se puede acceder a través de un navegador de escritorio. Si has experimentado una pérdida de datos generados por la aplicación en tu teléfono móvil, intenta acceder a la aplicación desde tu navegador de escritorio, iniciando sesión en la interfaz web de la aplicación con tus credenciales para esa aplicación. Es posible que encuentres los datos que perdiste en la interfaz del navegador.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech_assistance_end)

### ugd_phone

> Los datos generados por el usuario son el tipo de datos que creas o generas a través de una aplicación específica, y en caso de pérdida de datos, querrás verificar si esa aplicación tiene alguna configuración de respaldo habilitada de manera predeterminada o tiene alguna forma de recuperarla. Por ejemplo, si usas WhatsApp en tu móvil y faltan las conversaciones o si ocurre algo indeseado, puedes recuperar tus conversaciones si habilitaste la configuración de recuperación de WhatsApp, o si estás usando una aplicación para crear y mantener notas con información confidencial o personal, también puedes tener una opción de copia de seguridad habilitada sin que lo sepas.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech_assistance_end)

### android_phone

> Google tiene un servicio incorporado en Android, llamado Android Backup Service. De forma predeterminada, este servicio realiza copias de seguridad de varios tipos de datos y los asocia con el servicio de Google adecuado, los cuales también puedes acceder a través de la web. Puedes ver tu configuración de sincronización dirigiéndote a Configuración > Cuentas > Google, luego seleccionando tu dirección de Gmail. Si perdiste datos de algún tipo que estabas sincronizando con tu cuenta de Google, probablemente podrás recuperarlos ingresando a tu cuenta a través de un navegador web.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#phone_which_data)


### tech_assistance_end

> Si tu pérdida de datos ha sido causada por daños físicos, como por ejemplo, si tus dispositivos cayeron al piso o al agua, estuvieron expuestos a un corte de energía eléctrica u otros problemas, lo más probable es que deberás realizar una recuperación de datos almacenados en los discos duros. Si no sabes cómo realizar estas operaciones, debes comunicarte con una persona de TI con el equipo necesario de mantenimiento de hardware y equipo electrónico que podría ayudarte. Sin embargo, dependiendo del contexto y la sensibilidad de los datos que necesitas recuperar, no aconsejamos contactar a ninguna tienda de TI, y en lugar de eso intentar dar preferencia a las personas de TI que conozcas y en als que confíes.


### device_lost_theft_end

> En caso de dispositivos perdidos o robados, asegúrate de cambiar todas tus contraseñas lo antes posible y visitar nuestro [recurso específico sobre qué hacer si se pierde un dispositivo](../../lost-device).
>
> Si eres miembro de la sociedad civil y necesitas asistencia para adquirir un nuevo dispositivo para reemplazar el perdido, puedes comunicarse con las organizaciones que se enumeran a continuación.

:[](organisations?services=equipment_replacement)


### storage_devices_end

> Para recuperar los datos perdidos, recuerda que el tiempo es importante. Por ejemplo, recuperar un archivo que eliminaste accidentalmente unas pocas horas o un día antes podría tener mayores tasas de éxito que un archivo que perdiste meses antes.
>
> Considera utilizar una herramienta de software para intentar recuperar los datos que has perdido (debido a la eliminación o la corrupción) como [Recuva para Windows](https://www.ccleaner.com/recuva/download), y para Gnu / Linux Puedes investigar los diferentes programas disponibles para tu distribución. Ten en cuenta que estas herramientas no siempre funcionan, ya que tu sistema operativo puede haber escrito nuevos datos sobre la información eliminada. Debido a esto, debes hacer lo menos posible con tu computadora para eliminar un archivo e intentar restaurarlo.
>
> Para aumentar tus posibilidades de recuperar datos, deja de usar tus dispositivos inmediatamente. La escritura continua en el disco duro puede disminuir las posibilidades de encontrar y restaurar datos.
>
> Si ninguna de las opciones anteriores te ha funcionado, la situación más probable es que deberás realizar una recuperación de los datos almacenados en los discos duros. Si no sabes cómo realizar estas operaciones, debe comunicarte con una persona de TI de mantenimiento de hardware y equipo electrónico que podría ayudarte. Sin embargo, dependiendo del contexto y la sensibilidad de los datos que necesitas recuperar, no aconsejamos contactar a ninguna tienda de TI, y en lugar de eso intentar dar preferencia a las personas de TI que conozcas y confíes.

### resolved_end

Esperamos que esta guía de DFAK haya sido útil. Por favor danos tu opinión [vía email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Copias de seguridad: además de las diferentes sugerencias anteriores, siempre es una buena idea asegurarse de tener copias de seguridad, varias copias de seguridad diferentes, que almacenes en un lugar que no sea el mismo lugar donde se encuentran tus datos. Dependiendo del contexto, opta por almacenar copias de seguridad en servicios en la nube y en dispositivos físicos externos que se mantengan desconectados de tu computadora mientras se conecta a Internet.
- Para ambos tipos de copias de seguridad, debes proteger tus datos usando cifrado. Realiza copias de seguridad periódicas e incrementales para tus datos más importantes, y verifica que las tengas listas antes de realizar actualizaciones de software o de sistema operativo.
- Configura un sistema de carpetas estructurado - Cada persona tiene su propia forma de organizar sus datos e información importantes, no hay una solución única para todos. No obstante, es importante que consideres configurar un sistema de carpetas que se adapte a tus necesidades. Al crear un sistema de carpetas consistente, puedes hacer tu vida más fácil al saber mejor cuáles carpetas y archivos se deben respaldar con frecuencia, por ejemplo, en donde se encuentre la información importante en la que estás trabajando, en donde debes mantener los datos que contienen información personal o confidencial, tus colaboradores, y así sucesivamente. Como de costumbre, respira hondo y toma un tiempo para planificar el tipo de datos que produces o administras, y piensa en un sistema de carpetas que puedas hacer más coherente y ordenado.

#### resources

- [Access Now Helpline Community Documentation: Secure back up](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a box: Recuperar información perdida](https://securityinabox.org/es/guide/backup/)
- [Página oficial sobre copias de seguridad para Mac](https://support.apple.com/es-lamr/mac-backup)
- [Copias de seguridad y restauración en Windows 10](https://support.microsoft.com/es-us/help/4027408/windows-10-backup-and-restore)
- [Cómo habilitar copias de seguridad regulares en el iPhone](https://support.apple.com/es-lamr/HT203977)
- [Cómo habilitar copias de seguridad regulares en Android](https://support.google.com/android/answer/2819582?hl=es).
