---
layout: page
title: "Mi sitio web está caído, ¿qué está pasando?"
author: Rarenet
language: es
summary: "Una amenaza que enfrentan muchas ONG, medios de comunicación independientes y blogueros es que sus voces sean silenciadas porque su sitio web está caído o ha sido borrado."
date: 2019-08
permalink: /es/topics/website-not-working/
parent: /es/
---

# Mi sitio web está caído, ¿qué está pasando?

Una amenaza que enfrentan muchas ONG, medios de comunicación independientes y blogueros es silenciar sus voces porque su sitio web está caído o ha sido borrado. Este es un problema frustrante y puede tener muchas causas como un mal mantenimiento del sitio web, un proveedor de alojamiento no confiable, [script-kiddies](https://en.wikipedia.org/wiki/Script_kiddie), un ataque de "Denegación de Servicio" o una toma de posesión del sitio web. Esta sección del Kit de primeros auxilios digitales te guiará a través de algunos pasos básicos para diagnosticar problemas potenciales utilizando material de [Mi sitio web está inactivo (En Inglés)](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

Es importante saber que hay muchas razones por las que tu sitio web puede fallar. Estas pueden ir desde problemas técnicos de la empresa que aloja el sitio web hasta sistemas de gestión de contenido (CMS) no actualizados, como Joomla o Wordpress. Encontrar el problema y las posibles soluciones a este puede ser engorroso. Es una buena práctica **contactar a tu webmaster y a la empresa de alojamiento del sitio web** después del diagnóstico. Si ninguna de estas opciones está disponible para ti, [busca ayuda de una organización en la que confíes](questions/website_down_end).

## Consider

Para empezar, considera:

- ¿Quién levantó/programó tu sitio web? ¿Están disponibles para ayudar?
- ¿Fue construido usando Wordpress u otra plataforma popular de CMS?
- ¿Quién es tu proveedor web? Si no lo sabes, puedes usar una herramienta [como esta](http://www.whoishostingthis.com/) para descubrirlo.

## Workflow

### error_message

¿Estás viendo mensajes de error?

- [Sí, estoy viendo mensajes de error](#error_message_yes)
- [No](#hosting_message)

### hosting_message

¿Estás viendo un mensaje de tu proveedor web?

- [Sí, veo un mensaje de mi proveedor](#hosting_message_yes)
- [No](#site_not_loading)

### site_not_loading

¿Tu sitio no se carga en absoluto?

- [Si, el sitio web no carga en absoluto](#site_not_loading_yes)
- [No, el sitio web esta cargando](#hosting_working)

### hosting_working

¿El sitio web de tu proveedor funciona, pero tu sitio web no está disponible?

- [Sí, puedo acceder al sitio web de mi proveedor](#hosting_working_yes)
- [No](#similar_content_censored)

### similar_content_censored

¿Puedes visitar otros sitios con contenido como el de tu sitio web?

- [Tampoco puedo visitar otros sitios web con contenido similar.](#similar_content_censored_yes)
- [Otros sitios funcionan bien. Simplemente no puedo visitar mi sitio](#loading_intermittently)

### loading_intermittently

¿Se está cargando tu sitio web de forma intermitente o inusualmente lenta?

- [Sí, mi sitio se está cargando intermitentemente o está lento](#loading_intermittently_yes)
- [No, mi sitio web se está cargando, pero podría haber sido hackeado](#website_defaced)

### website_defaced

¿Tu sitio web se carga pero la apariencia y el contenido no son lo que esperas ver?

- [Sí, mi sitio no tiene el contenido/aspecto esperado](#defaced_attack_yes)
- [No](#website_down_end)

### error_message_yes

> Esto podría ser un **_problema de software_**: debes pensar acerca de cualquier cambio reciente que tu o tu equipo hayan realizado y comunicarte con tu webmaster. Envía a tu webmaster una captura de pantalla, el enlace de la página con la que está teniendo problemas y cualquier mensaje de error que veas, esto le ayudará a descubrir cuál podría ser la causa del problema. También puedes copiar los mensajes de error en un motor de búsqueda para ver si hay soluciones fáciles.

Ha ayudado esto?

- [Sí](#resolved_end)
- [No](#website_down_end)

### hosting_message_yes

> El sitio podría haber sido bloqueado por razones legales, [violaciones a derechos de autor (En Inglés)](https://www.eff.org/issues/bloggers/legal/liability/IP), facturación u otras razones. Ponte en contacto con tu proveedor para obtener más detalles sobre por qué suspendieron tu alojamiento web.

Ha ayudado esto?

- [Sí](#resolved_end)
- [No, necesito soporte legal](#legal_end)
- [No, necesito soporte técnico](#website_down_end)

### site_not_loading_yes

> Tu proveedor de alojamiento puede estar teniendo problemas, en cuyo caso puede estar enfrentando un **_problema de hosting_**. ¿Puedes visitar el sitio web de tu empresa de alojamiento? Ten en cuenta que esto **no** es la sección de administración de tu propio sitio, sino el de la empresa u organización con la que trabajas para alojar tu sitio.
>
> Busca un blog de "estado" de tu proveedor (por ejemplo, https://www.dreamhoststatus.com/), y también busca en [twitter.com](https://twitter.com) para ver a otros usuarios que discuten la potencial inactividad de la empresa de alojamiento. Una búsqueda simple como "(nombre de la compañía) down" o "(nombre de la compañía) caido/a" a menudo puedes revelar Si otros tienen el mismo problema.

Ha ayudado esto?

- [Sí](#resolved_end)
- [No, el sitio web de mi proveedor no está caído](#hosting_working_yes)
- [No, necesito soporte técnico](#website_down_end)

### hosting_working_yes

> Verifica tu URL en [este sitio web](https://downforeveryoneorjustme.com/): tu sitio podría estar activo pero no puedes verlo.
>
> Si tu sitio está activo pero no puede verlo, es probable que este sea un **_problema de red_**: tu propia conexión a Internet podría tener problemas de acceso a tu sitio.

¿Necesitas más ayuda?

- [No](#resolved_end)
- [Sí, necesito ayuda para restaurar mi conexión de red](#website_down_end)
- [Sí, esto no es un problema de red y mi sitio web está inactivo para todos](#similar_content_censored)

### similar_content_censored_yes

> Intenta visitar sitios web con contenido similar a tu sitio web. Intenta también usar [Tor](https://www.torproject.org/projects/gettor.html) o [Psiphon](https://psiphon.ca/products.php) para acceder a tu sitio.
>
> Si puedes visitar tu sitio a través de Tor o Psiphon, tienes un **_problema de censura_**. Todavía estás en línea para otras partes del mundo, pero estás siendo censurado en su propio país.

¿Te gustaría hacer algo respecto a esta censura?

- [Sí, me gustaría informar esto públicamente y necesito apoyo para mi campaña de defensa](#advocacy_end)
- [Sí, me gustaría encontrar una solución para que mi sitio web sea accesible](#website_down_end)
- [No](#resolved_end)

### loading_intermittently_yes

> Tu sitio puede verse abrumado por la cantidad y la velocidad de las solicitudes de páginas que recibe, esto es un **_problema de rendimiento_**.
>
> Esto podría ser "bueno", ya que tu sitio se ha vuelto más popular y simplemente necesita algunas mejoras para responder a más lectores; verifica el análisis del sitio para ver un patrón de crecimiento a largo plazo. Ponte en contacto con tu webmaster o proveedor de hosting para obtener orientación. Muchos blogs populares y plataformas de CMS (Joomla, Wordpress, Drupal ...) tienen complementos para ayudar a almacenar en caché tu sitio web localmente e integrar una CDN, lo cual puede mejorar dramáticamente el rendimiento y la capacidad de recuperación del sitio. Muchas de las soluciones a continuación también pueden ayudar con problemas de rendimiento.
>
> Si experimentas un **problema de rendimiento** grave, tu sitio puede ser víctima de un [** "ataque de denegación de servicio distribuido"**](https://ssd.eff.org/es/glossary/ataque-de-denegaci%C3%B3n-de-servicio-distribuido) (o DDoS). Sigue los pasos a continuación para mitigar un ataque de este tipo:
>
> - Paso 1: ponte en contacto con una persona de confianza que pueda ayudarte con tu sitio web (tu webmaster, las personas que te ayudaron a configurar el sitio, personal interno o tu proveedor de hosting).
>
> - Paso 2: trabaja con la compañía a la que le compraste tu nombre de dominio y cambia el "Time to Live" o TTL a 1 hora (puedes encontrar instrucciones sobre cómo hacerlo en los sitios web de muchos proveedores, como [Network Solutions](http://www.networksolutions.com/support/how-to-manage-advanced-dns-records/) o [GoDaddy](https://cl.godaddy.com/help/administrar-dns-680)). Esto puede ayudarte a redirigir tu sitio mucho más rápido una vez atacado (el valor predeterminado es 72 horas o tres días). Esta configuración se encontrará a menudo en las propiedades "avanzadas" de tu dominio, a veces como parte de los registros de servicio o SRV.
>
> - Paso 3: Mueve el sitio a un servicio de mitigación DDoS. Para comenzar:
>
> - [Deflect.ca](https://deflect.ca/)
> - [Project Shield de Google](https://projectshield.withgoogle.com/landing?hl=es)
> - [Proyecto Galileo de CloudFlare](https://www.cloudflare.com/galileo)
>
> Para obtener una lista completa de las organizaciones de confianza que pueden ayudar a mitigar un ataque DDoS, consulta [esta sección](#ddos_end)
>
> - Paso 4: Tan pronto como hayas recuperado el control, revisa tus opciones y decide entre un proveedor seguro o simplemente continua con tu servicio de mitigación DDoS.

Para obtener una lista completa de organizaciones confiables que pueden proporcionar un alojamiento seguro, mira [esta sección](questions/web_hosting_end)

### defaced_attack_yes

> El "defacement" de un sitio web es una práctica en la que un atacante reemplaza el contenido o la apariencia visual del sitio web con su propio contenido. Estos ataques generalmente se llevan a cabo mediante la explotación de vulnerabilidades en plataformas de CMS sin mantenimiento, sin las últimas actualizaciones de seguridad o mediante el uso de nombres de usuario y contraseñas de cuentas de alojamiento robadas.
>
> - Paso 1: Comprueba que se trata de un cambio malicioso de tu sitio web. Una práctica lamentable pero legal es comprar nombres de dominio caducados recientemente para "hacerse cargo" del tráfico que tenían con fines publicitarios. Es muy importante mantener los pagos de tu nombre de dominio al día.
> - Paso 2: Si tu sitio web ha sido modificado, primero recupera el control de tu cuenta de inicio de sesión y restablece tu contraseña, consulta la sección de secuestro de cuentas para obtener ayuda.
> - Paso 3: Haz una copia de seguridad del sitio que luego se puede usar para investigar el "defacement".
> - Paso 4: Desactiva temporalmente tu sitio web: usa una página de destino simple (landing page) o una página de 'parking'.
> - Paso 5: Determina cómo fue hackeado tu sitio. Tu proveedor puede ser capaz de ayudar. Los problemas comunes incluyen a las partes más antiguas de tu sitio con scripts/herramientas personalizadas que se ejecutan en ellos, sistemas de administración de contenido obsoletos y programación personalizada con fallas de seguridad.
> - Paso 6: Restaura tu sitio original a partir de copias de seguridad. Si ni siquiera tu ni tu compañía de hosting tienen copias de seguridad, es posible que tengas que reconstruir el sitio web desde cero. También ten en cuenta que si tus únicas copias de seguridad están en posesión tu proveedor, un atacante puede eliminarlas cuando tome el control de tu sitio.

¿Han ayudado estas recomendaciones?

- [Sí](#resolved_end)
- [No](#website_down_end)

### website_down_end

> Si aún necesitas ayuda después de todas las preguntas que respondiste, puedes ponerte en contacto con una organización de confianza y solicitar asistencia.
>
> Antes de ponerte en contacto, házte las siguientes preguntas:
>
> - ¿Cómo está estructurada y sostenida la empresa/organización? ¿Qué tipos de investigación o informes se deben hacer, si los hay?
> - Considerar en qué país/países tienen presencia legal y en cuáles deberían cumplir con la ley y otras solicitudes legales
> - ¿Qué registros se crean y por cuánto tiempo están disponibles?
> - ¿Existen restricciones con respecto al tipo de contenido que alojará el servicio/proxy y qué impacto podrían tener en tu sitio?
> - ¿Existen restricciones en los países donde pueden brindar servicios?
> - ¿Aceptan alguna forma de pago que puedas usar? ¿Se puede costear el servicio?
> - Comunicaciones seguras: debes poder iniciar sesión de forma segura y comunicarte con el proveedor de servicios de forma privada.
> - ¿Existe una opción para la autenticación de dos factores, para mejorar la seguridad del acceso del administrador? Estas u otras políticas de acceso seguro similares pueden ayudar a reducir la amenaza de otras formas de ataques contra tu sitio web.
> - ¿A qué tipo de soporte continuo tendrás acceso? ¿Existe un costo adicional para el soporte técnico y/o recibirás suficiente ayuda si estás utilizando un nivel "gratuito"?
> - ¿Puedes "probar" tu sitio web antes de migrarlo a través de un sitio de prueba?

Aquí hay una lista de organizaciones que pueden ayudarte con tu problema:

:[](organisations?services=web_protection)

### legal_end

> Si tu sitio web está caído debido a razones legales y necesitas asistencia, comunícate con una organización que pueda ayudarte:

:[](organisations?services=legal)

### advocacy_end

> Si deseas recibir apoyo para lanzar una campaña contra la censura, pónte en contacto con organizaciones que pueden ayudarte con los esfuerzos de incidencia:

:[](organisations?services=advocacy)

### ddos_end

> Si necesitas ayuda para mitigar un ataque DDoS contra tu sitio web, consulta las organizaciones especializadas en la mitigación de este tipo de ataques:

:[](organisations?services=ddos)

### web_hosting_end

> Si estás buscando una organización confiable para alojar tu sitio web en un servidor seguro, consulta la siguiente lista:
>
> Antes de ponerte en contacto con estas organizaciones, piensa en estas preguntas:
>
> - ¿Ofrecen soporte completo para migrar tu sitio a su servicio?
> - ¿Los servicios son iguales o mejores que los de tu proveedor actual, al menos para las herramientas/servicios que usas? Las mejores cosas para comprobar son:
> - Paneles de administración como cPanel
> - Cuentas de correo electrónico (cuántos, cuotas, acceso a través de SMTP, IMAP)
> - Bases de datos (cuántos, tipos, acceso)
> - Acceso remoto a través de SFTP/SSH
> - Soporte para lenguajes de programación (PHP, Perl, Ruby, cgi-bin access ...) o CMS (Drupal, Joomla, Wordpress ...) que usa el sitio

Aquí hay una lista de organizaciones que pueden ayudarte con el alojamiento web:

:[](organisations?services=web_hosting)

### resolved_end

Esperamos que esta guía del Kit de primeros auxilios digitales (DFAK) te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto: incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Copias de seguridad** - Además de los servicios y sugerencias a continuación, siempre es buena idea asegurarse de tener copias de seguridad (que se almacenen en un lugar que no sea el mismo en donde se encuentra tu sitio web). Muchos proveedores de alojamiento y plataformas de sitios web tienen esto incluido, pero es mejor tener copias adicionales sin conexión.

- **Manten el software actualizado** - Si estás utilizando un sistema de gestión de contenidos (CMS) como WordPress o Drupal, asegúrate de que los componentes de tu sitio web estén actualizados al software más reciente, especialmente si han habido actualizaciones de seguridad.

- **Monitoreo** - Hay muchos servicios que pueden revisar constantemente tu sitio y te pueden enviar mensajes de correo electrónico o mensajes de texto si se produce una caída. [Este artículo de Mashable (En Inglés)](http://mashable.com/2010/04/09/free-uptime-monitoring/) enumera los 10 más populares. Ten en cuenta que el correo electrónico o el número de teléfono que utilizas para la supervisión estarán claramente asociados con la administración del sitio web.

#### Resources

- [EFF: Keeping your site alive](https://www.eff.org/keeping-your-site-alive)
- [CERT.be: DDoS proactive and reactive measures](https://www.cert.be/en/paper/ddos-proactive-and-reactive-measures)
- [Sucuri: What is a DDoS Attack?](​​​​​​​https://sucuri.net/guides/what-is-a-ddos-attack/)
